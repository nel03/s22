/*
    Create functions which can manipulate our arrays.
    You may change the names on the initial list before you use the array varaibles.
*/

let registeredUsers = [

    "James Jeffries",
    "Maggie Williams",
    "Macie West",
    "Michelle Queen",
    "Angelica Smith",
    "Fernando Dela Cruz",
    "Mike Dy"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/
    function addUser(newUser){
        let userExists = registeredUsers.includes(newUser);

        if(userExists){
            alert(newUser + 'Registration failed. Username already exists!')
        }else{
            registeredUsers.push(newUser)
            alert('Thank you for registering!')
        }
    }

    addUser('Spartan from Hell')
    console.log('Add new user: ')
    console.log(registeredUsers);

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/


        function addFriend(user) {
          
          const users = registeredUsers.toString();
          if (users.includes(user)) {
            friendsList.push(user);
            alert("You have added " + user + " as a friend!");
          } else {
            alert("User not found");
          }
          console.log("friendsList");
          console.log(friendsList);
        }

        addFriend('Maggie Williams');
        addFriend('Michelle Queen');
        addFriend('Mike Dy');






/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
        console.log('Shows the List of Friends Individualy')
        friendsList.forEach(function(friend){
            if(friendsList === 0){
                alert('You currently have 0 friends. Add one first.')
            }else{
            console.log(friend)
            }
        })


// STRETCH GOALS

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/

        let countFriend = 0;
        function displayFriends() {
          countFriend++;

          if (countFriend < 2) {
            return;
          }

          if (!friendsList.length) {
            alert("You currently have 0 friends. Add one first.");
            return;
          }

          friendsList.forEach((friend) => {
            console.log(friend);
          });
        }

        displayFriends();



/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/
    
    function removedFriend(){
        let lastFriend = friendsList[friendsList.length-1];
        friendsList.pop()
        if(friendsList === 0){
             alert('You currently have 0 friends. Add one first.')
        } return lastFriend;
    };
    lastFriend = removedFriend();
        console.log(lastFriend + ' was removed from your friend list')
        console.log(friendsList)

/*======================================================================================*/



// Try this for fun:
/*
   

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/
    // function remFriend(){
    //     let spliceFriend = friendsList.splice(1,4);
    //     if(friendsList === 0){
    //          alert('You currently have 0 friends. Add one first.')
    //     } return spliceFriend;
    // };
    // spliceFriend = remFriend();
    //     console.log(spliceFriend + ' was removed from your friend list')
    //     console.log(friendsList)