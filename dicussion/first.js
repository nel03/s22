// console.log ('Hi')

/*
	Array Methods

	Mutator Methods
		> seeks to modify the contents of an array
		> are function that mutate an array after they are created. these methods manipulate original array performing various tasks such as adding or removing elements
*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Watermelon'];
	console.log('Current Fuits Array:');
	console.log(fruits);
/*
	push()
		> adds an element in the end of an array and returns the array's length

	Syntax:
		arrayName.push(element)
*/

	console.log('Adding elements');

fruits.push('Mango');
	console.log(fruits);

let fruitsLength = fruits.push('Melon');
	console.log(fruits);
	console.log(fruitsLength);

fruits.push('Avocado', 'Guava');
	console.log(fruits);

/*
	pop()
		> removed the last element in our array and returns the removed element (when applied inside a variable)

	Syntax:
		arrayName.pop()
*/

let removedFruit = fruits.pop();
	console.log('shows the removed element using the pop() method')
	console.log(removedFruit);

/*
	unshift()
		> adds one or more elements at the beginning of an array
		> returns the length of the array (when presented insede a variable)

	Syntax:
		arrayName.unshift(elementA, elementB)
*/

fruits.unshift('Lime', "Banana");
	console.log('Mutated array from the unshift method:')
	console.log(fruits);

/*
	shift()
		>removes an element at the beginning of our array and return the removed element

	Syntax
		arrayName.shift()
*/

let removedFruit2 = fruits.shift();
	console.log('Mutated array from the shift method:')
	console.log(removedFruit2);
	console.log(fruits);

/*
	splice()
		> allows to simultaneously remove elements from a specified index number and adds an element

	Syntax
		arrayName.splice(stardingIndex, deleteCount, elementsToBeAdded)
*/

let fruitsSplice = fruits.splice(1, 2, 'Cherry', 'Lychee'); // 1 is the index start, 2 is the elements removed, cherry and lychee is the elements added
	console.log('Mutated arrays from splice method:');
	console.log(fruitsSplice); // shows the element deleted
	console.log(fruits);

let removedSplice = fruits.splice(3,2); // using splice without adding elements
	console.log(removedSplice);
	console.log(fruits);

/*
	sort()
		> reannges the array element in alphanumeric order

	Syntax:
		arrayName.sort();
*/

fruits.sort();
	console.log('Rearranges the array element in alphanumeric order using sort method:')
	console.log(fruits);

let mixedArr = [12, 'May', 36, 94, 'August', 5, 6.3, 94, "September", 10, 100, 1000];
	console.log(mixedArr.sort());

/*
	reverse()
		> reverses the order or the element in an array

	Syntax:
		arrayName.reverse()
*/

fruits.reverse();
	console.log('Mutated array from reverse method:')
	console.log(fruits);

// for sorting the items in descending order
fruits.sort().reverse()
	console.log('using sort then reverse')
	console.log(fruits);

//mini activity
/*
	 - Debug the function which will allow us to list fruits in the fruits array.
	 	-- this function should be able to receive a string.
	 	-- determine if the input fruit name already exist in the fruits array.
	 		*** If it does, show an alert message: "Fruit already listed on our inventory".
	 		*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
	 	-- invoke and register a new fruit in the fruit array.
	 	-- log the updated fruits array in the console
*/

	 	function registerFruit(fruitName) {

	 		let doesFruitExist = fruits.includes(fruitName);

	 		if(doesFruitExist) {
	 			// alerat(fruitName + "is already on our inventory")
	 		} else {
	 			fruits.push(fruitName);
	 			
	 			// alert("Fruit is now listed in our inventory")
	 		}
	 	}
	 	
	registerFruit('Star Apple')
	console.log (fruits)

//miniActivity 2

let miniActivity = [];
	console.log('Shows an Empty Array');
	console.log (miniActivity);

miniActivity.push('Ginn', 'Empe', 'Beer', 'Double Light', 'Red Win', 'Coke');
	console.log('Adding elements on an empty Array:')
	console.log(miniActivity);

miniActivity.unshift('Sprite', 'Water');
	console.log('adding two items at the beginning using unshift');
	console.log(miniActivity);

miniActivity.pop();
	console.log('Remove one item at the end of the array using pop:');
	console.log(miniActivity);

miniActivity.shift();
	console.log('Remove one item at the beginning of the array using shift:');
	console.log(miniActivity);

miniActivity.sort();
	console.log('arrange the elements in alphanumeric using sort:')
	console.log(miniActivity)


/*
	non-mutator methods

		> these are function that do not modify or change an array after they are created.
		> the methods do not manipulate the original array performing various task as returning elements from an array and combining arrays and printing the output

		Stynax:

*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
	console.log(countries);

/*
	indexof()
		>returns the index number of the first matching element found in an array. if no match is found, the result will be -1. the search process will be done from our first element proceeding to the last element

	Syntax:
		arrayName.indexOf(seachValue)
		arrayName.indexOf(seachValue, fromIndex)
*/

let firstIndex = countries.indexOf('PH'); // the first item
	console.log('Result of indexOf method: ' + firstIndex);

firstIndex = countries.indexOf('PH', 4); // start counting from 4 the search for PH if not found result will be -1
	console.log('Result of indexOf method: ' + firstIndex);

firstIndex = countries.indexOf('PH', 7);
	console.log('Result of indexOf method: ' + firstIndex);

firstIndex = countries.indexOf('PH', -1);
	console.log('Result of indexOf method: ' + firstIndex);

/*
	lastIndexOf()
		> returns the index number of the last matching element found in an array. The search process will be done from the last element proceeding to the first

	Syntax:
		arrayName.lastIndexOf(searchValue)
		arrayName.lastIndexOf(searchValue, fromIndex)
*/

let lastIndex = countries.lastIndexOf('PH'); //start the counting at the end of the array
	console.log('Result of lastIndexOf method: ' + lastIndex);

lastIndex = countries.lastIndexOf('PH', 4);
	console.log('Result of lastIndexOf method: ' + lastIndex);	

/*
	slice()
		> slices elements from our array and returns a new array

	Syntax:
		arrayName.slice(startingIndex)
		arrayName.slice(startingIndex, endingIndex)
*/

	console.log(countries);

let slicedArrA = countries.slice(3)
	console.log('Result from Slice Method:');
	console.log(slicedArrA);
	console.log(countries);

let slicedArrB = countries.slice(2,5);
	console.log('Result from Slice Method:');
	console.log(slicedArrB);
	console.log(countries);

let slicedArrC = countries.slice(-3) // start at the end of the array
	console.log('Result from Slice Method:');
	console.log(slicedArrC);
	console.log(countries);


/*
	toString()
		> returns an array as a string separated by commas
		> is used internally by JS when an object/ array needs to be displayed as a text(like in HTML), or when an object/array needs to be used as a string

	Syntax:
		arrayName.toString()
*/

let stringArray = countries.toString();
	console.log('Result fom toString Method:');
	console.log(stringArray);


/*
	concat()
		> combines two or more arrays and returns the combined result

	Syntax: 
		arrayA.concat(arrayB)
		arrayA.concat(elementA)
*/

let taskArrayA = ['Drink HTML', 'Eat JavaScript'];
let taskArrayB = ['Inhale CSS', 'Breath Sass'];
let taskArrayC = ['Get Git', 'Be Node'];

let tasks = taskArrayA.concat(taskArrayB);
	console.log('Result from Concat Method:')
	console.log(tasks);

let allTask = taskArrayA.concat(taskArrayB, taskArrayC);
	console.log(allTask);

let combineTask = taskArrayA.concat('smell Express', ' throw react')
	console.log(combineTask); //combining array with element similar with push()

/*
	join()
		> returns an array as a string
		> does not change the original array
		> we can use any separator. the default is comma(,)

	Syntax:
		arrayName.join(separatorString)
*/

let students = ['Elysha', 'Gab', 'Ronel', 'Jean'];
	console.log(students.join())
	console.log(students.join(' '))
	console.log(students.join(" \\ "))
	console.log(students.join(" - "))
	console.log(students.join(" / "))

/*
	Iteration Methods
		> are loops designed to perform repitive tasks in an array. used for manipulating array data resulting in complex tasks.
		> noramally work with a function supplied as an argument
		> aims to evaluate each element in an array

	forEach()
		>similar to for loop that iterates on each array element

	Syntax:
		arrayName.forEach(function(individualElement){
			statement
		})
*/

allTask.forEach(function(task){
	console.log(task);
});

//using for each with conditional statements

let filteredTasks = []

allTask.forEach(function(task){
	console.log(task)
	if (task.length > 10) {
		filteredTasks.push(task);
	};
});

	console.log(allTask);
	console.log('result of filteredTasks:')
	console.log(filteredTasks);


/*
	map()
		> iterates on each element and returns a new array with different values depending on the result of the function's operation

	Syntax:
		arrayName.map(fucntion(individualElement){
			statement
		})
*/


let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number){
	console.log(number)
	return number*number
})

	console.log('Original Array')
	console.log(numbers)
	console.log('Result of the Map Method;')
	console.log(numberMap);


/*
	every()
		> checks if all elements in an array met the given condition. returns a tru value if all elements meet the condition and false if otherwise

	Syntax:
		arrayName.every(fucntion(individualElement){
			return expression/condition
		})
*/

let allValid = numbers.every(function(number){
	return (number < 3);
});

	console.log('Result of every Method')
	console.log(allValid)


/*
	some()
		> checks if at least one element in the array meet the given condition. Returns a true value if at least one of the elements meets the given condition and false if otherwise.
	Syntax:
		arrayName.some(function(individual element){
			return expression/condition
		})
*/

let someValid = numbers.some(function(number){
	return(number < 3)
})
	console.log('Result of sum Method')
	console.log(someValid)


/*
	filter()
		> returns a new array that contains elements which meets the given condition. Return an empty array if no elementswere found(that satisfied the given condition)

	Syntax: 
		arrayName.filter(function(individualElement){
				return expression/condition
		})
*/

let filteredValid = numbers.filter(function(number){
	return (number < 3);
})

	console.log('Result of filter Method')
	console.log(filteredValid);


/*
	includes()
		> checks if the argument passed can be found in an array
		> can be chained after another method. the result of the first method is used on the second method until all chained methods have been resolved
*/

let products = ['mouse', 'KEYBOARD', 'laptop', 'monitor'];

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a')
})

	console.log('Result of includes Method')
	console.log(filteredProducts);
